<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	protected $fillable = ['customer_id', 'amount'];
	public $timestamps = true;

	/**
	 * Get the customer record associated with the transaction.
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function customer(  ) {
		return $this->belongsTo(Customer::class);
	}
}
