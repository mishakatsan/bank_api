<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller {

	/**
	 * create customer
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store( Request $request ) {
		$validator = Validator::make( $request->all(), [
			'name' => 'required|min:2|max:190',
			'cnp'  => 'required|boolean',
		] );

		if ( $validator->fails() ) {
			return response()->json( $validator->errors() );
		}

		$customer = Customer::create( [ 'name' => $request->name, 'cnp' => $request->cnp ] );

		return response()->json( [ 'customerId' => $customer->id ] );
	}
}
