## Installation
--------------

Setting up your development environment on your local machine:
```
$ git clone https://mishakatsan@bitbucket.org/mishakatsan/bank_api.git
$ cd bank_api
$ composer install
$ php artisan key:generate
```
Create and configure your .env file.
```
$ php artisan migrate --seed
$ php artisan passport:install
$ php artisan serve
```
Cron start command
```
$ 47 23 */2 * * /path/to/php path/to/project/artisan statistic:daily
```
## Before starting
----------------
In your browser you can register user
