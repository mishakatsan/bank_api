<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command( 'statistic:daily', function () {
	$this->comment( 'start' );

    $yesterday = \Carbon\Carbon::now()->subDay()->format( 'Y-m-d' );
	$sum      = \App\Transaction::whereDate( 'created_at', $yesterday )->sum( 'amount' );

	\App\Statistic::create( [ 'amount' => $sum, 'date' => $yesterday ] );

	$this->comment( 'done' );
} )->describe( 'Daily statistic' );
