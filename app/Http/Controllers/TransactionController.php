<?php

namespace App\Http\Controllers;

use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller {

	/**
	 * return all transactions
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index() {
		$transactions = Transaction::all();

		$transactions = $transactions->map( function ( $item ) {
			$item->customer;

			return $item;
		} );

		return response()->json( $transactions );
	}

	/**
	 * return one transaction
	 * @param $customerId
	 * @param $transactionId
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show( $customerId, $transactionId ) {
		$transaction = Transaction::where( 'id', $transactionId )
		                          ->where( 'customer_id', $customerId )
		                          ->first();

		if ( $transaction == null ) {
			return response()->json( 'Error, not possible to find transaction by parameters, check your parameters' );
		}

		return response()->json( [
			'transactionId' => $transaction->id,
			'amount'        => $transaction->amount,
			'date'          => $transaction->created_at->format( 'Y-m-d' ),
		] );
	}

	/**
	 * return filtered transactions by parameters
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function filter( Request $request ) {

		$validator =  Validator::make( $request->all(), [
			'customerid' => 'numeric',
			'amount'     => 'numeric',
			'offset'     => 'numeric',
			'limit'     => 'numeric',
			'date'     => 'date',
		] );

		if ($validator->fails()) {
			return response()->json( $validator->errors() );
		}

		$amount = $request->amount;
		$date   = Carbon::parse( $request->date )->format( 'Y-m-d' );
		$offset = $request->offset;
		$limit  = $request->limit;

		$transactions = Transaction::where( 'customer_id', $request->customerid )
		                           ->where( 'amount', $amount )
		                           ->whereDate( 'created_at', $date )
		                           ->limit( $limit )
		                           ->skip( ( $offset - 1 ) * $limit )
		                           ->get();

		if ( $transactions == null ) {
			return response()->json( 'Error, not possible to find transaction by parameters, check your parameters' );
		}

		return response()->json( $transactions );
	}

	/**
	 * create new transaction
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function create( Request $request ) {
		$validator =  Validator::make( $request->all(), [
			'customerId' => 'required|numeric',
			'amount'     => 'required|numeric',
		] );

		if ($validator->fails()) {
			return response()->json( $validator->errors() );
		}

		$transaction = Transaction::create( [ 'customer_id' => $request->customerId, 'amount' => $request->amount ] );

		return response()->json( [
			'transactionId' => $transaction->id,
			'customerId'    => $transaction->customer_id,
			'amount'        => $transaction->amount,
			'date'          => $transaction->created_at->format( 'Y-m-d' ),
		] );
	}

	/**
	 * update transaction amount by transactionId
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update( Request $request ) {
		$validator =  Validator::make( $request->all(), [
			'transactionId' => 'required|numeric',
			'amount'        => 'required|numeric',
		] );

		if ($validator->fails()) {
			return response()->json( $validator->errors() );
		}

		$transaction         = Transaction::find( $request->transactionId );

		if ( $transaction == null ) {
			return response()->json( 'Error, not possible to find transaction by parameters, check your parameters' );
		}

		$transaction->amount = $request->amount;
		$transaction->save();

		return response()->json( [
			'transactionId' => $transaction->id,
			'customerId'    => $transaction->customer_id,
			'amount'        => $transaction->amount,
			'date'          => $transaction->created_at->format( 'Y-m-d' ),
		] );
	}

	/**
	 * delete transaction by transactionId
	 * @param $transactionId
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delete( $transactionId ) {
		$transaction = Transaction::find( $transactionId );
		if ( $transaction == null ) {
			return response()->json( 'failed' );
		}
		Transaction::destroy( $transactionId );

		return response()->json( 'success' );
	}
}
