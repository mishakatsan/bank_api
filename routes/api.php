<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('customer', 'CustomerController@store')->name('customer.store');

Route::get('transactions', 'TransactionController@index')->name('transactions.index');
Route::get('transaction/{customerId}/{transactionId}', 'TransactionController@show')->name('transaction.show');
Route::get('transaction', 'TransactionController@filter')->name('transaction.filter');
Route::post('transaction', 'TransactionController@create')->name('transaction.create');
Route::put('transaction', 'TransactionController@update')->name('transaction.update');
Route::delete('transaction/{transactionId}', 'TransactionController@delete')->name('transaction.delete');
